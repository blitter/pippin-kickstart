Pippin Kickstart
================

This is a very small utility for the Bandai Pippin that works around the console's built-in security, allowing a user to boot from a volume of their choice. Written for the GNU assembler and built using the [Retro68 toolchain](https://github.com/autc04/Retro68), but with minor modifications to the Makefile it should build using a 68K version of GNU binutils. Available under the GPLv2; distribution and improvements are encouraged.

Usage
-----

Boot your retail Bandai Pippin console using this CD. On supported systems, Pippin Kickstart will launch and automatically report the detected ROM version and available RAM. It will then eject this CD, after which the message "Finding boot candidate" will appear. During this time, Pippin Kickstart will look for a boot volume on connected drives and attempt to boot from one if found. If a removable volume is found that is not bootable, it will be ejected and the search will continue. The chosen boot volume need not be authenticated.

On Pippin systems with ROM 1.0 (most models with a white case), the SCSI Manager may be patched to allow for SCSI IDs other than 3 to be checked. This patch uses 35K of RAM; it may be skipped by holding down the mouse button as Pippin Kickstart launches.

Known Limitations
-----------------

I tested Pippin Kickstart with the Pippin's own internal CD-ROM drive, an external Toshiba SD-1401 DVD-ROM drive, and an external 1GB Quantum Fireball SCSI hard drive. Of these, only the external Toshiba optical drive refused to boot. This is likely because the Pippin is hardcoded to use the .AppleCD driver with its internal CD-ROM drive on SCSI ID 3, and the ID of any external SCSI optical drive would be unknown to Pippin Kickstart at runtime. The Toshiba drive will however boot the Pippin only if it is assigned SCSI ID 3, replacing the internal CD-ROM drive.

Thanks
------

- Pierre Dandumont for testing on ROM 1.3
- Josh Juran for being a sounding board
- The #mac68k crew for their ongoing support
- Elliot Nunn for his deep knowledge of mid-90s Mac ROMs
- Allison Reid for testing on ROM 1.0
- Katherine R. for additional testing on ROM 1.0
- Tommy Yune for his invaluable assistance and CD artwork

Build Process
-------------

1. Add m68k-apple-macos-as and m68k-apple-macos-ld to your PATH.
2. Run `make` from the Pippin Kickstart source directory.

If you get a warning about ".space, .nops or .fill with negative value," then the output binary is too large. Pippin Kickstart is padded to its maximum size of 1024 bytes.

Preparation
-----------

1. Build Pippin Kickstart.
2. Replace the boot blocks (first 1024 bytes) of a target HFS volume with the contents of PippinKickstart.bin.
3. Sign the target volume so it is bootable on an unmodified Bandai Pippin console.
4. Burn the target volume to CD.

Have fun.

Keith Kaisershot

12-29-20
